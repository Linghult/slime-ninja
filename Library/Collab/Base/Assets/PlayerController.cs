﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    KeyCode jumpKey = KeyCode.UpArrow;
    KeyCode moveLeft = KeyCode.LeftArrow;
    KeyCode moveright = KeyCode.RightArrow;

    // Basic movement variables
    public float speed;
    public float jumpForce;
    private float moveInput;

    private Rigidbody2D rb;

    private bool facingRight = true;

    // Ground check variables
    private bool isGrounded;
    public Transform groundCheck;
    public float checkRadius;
    public LayerMask whatIsGround;

    //Extra jump variables
    private int extraJumps;
    public int extraJumpsValue;

    // Better jump variables
    public float fallMultiplier;
    public float lowJumpMultiplier;

    // Wall jumping variables
    private bool isWallSliding;
    private bool touchingRightWall; // false = touching left wall, true = touching right wall
    public Transform leftWallCheck;
    public Transform rightWallCheck;
    public LayerMask whatIsWall;
    public float wallSlideSpeedMultiplier;

    // Use this for initialization
    void Start () {
        extraJumps = extraJumpsValue;
        rb = GetComponent<Rigidbody2D>();
	}

    void FixedUpdate() {
        moveInput = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);

        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);
        isWallSliding = (Physics2D.OverlapCircle(leftWallCheck.position, checkRadius, whatIsWall) || Physics2D.OverlapCircle(rightWallCheck.position, checkRadius, whatIsWall)) && !isGrounded;

        if(facingRight == false && moveInput > 0) {
            Flip();
        } else if(facingRight == true && moveInput < 0) {
            Flip();
        }
    }

    // Flips character model horizontally
    void Flip() {
        facingRight = !facingRight;
        Vector3 Scaler = transform.localScale;
        Scaler.x *= -1;
        transform.localScale = Scaler;
    }
	
	// Update is called once per frame
	void Update () {
        // Resets extra jumps
        if(isGrounded == true || isWallSliding == true)
        {
            extraJumps = extraJumpsValue;
        }
        
        // Jumping and extra jumping
        if(Input.GetKeyDown(jumpKey) && extraJumps > 0)
        {
            rb.velocity = Vector2.up * jumpForce;
            extraJumps--;
        }
        else if(Input.GetKeyDown(jumpKey) && extraJumps == 0 && isGrounded == true)
        {
            rb.velocity = Vector2.up * jumpForce;
        }

        // Better JUMP. Allow smaller jumps as well as faster falling depending on your settings. 
        if (rb.velocity.y < 0) // Faster falling
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
        else if (rb.velocity.y > 0 && !Input.GetKey(jumpKey)) // Smaller jumps
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
        }

        if(isWallSliding == true && isGrounded == false && moveInput != 0)
        {
            touchingRightWall = Physics2D.OverlapCircle(rightWallCheck.position, checkRadius, whatIsWall);
            if((facingRight && touchingRightWall) || !(!facingRight && !touchingRightWall)) // ¯\_(ツ)_/¯
            {
                if(rb.velocity.y < 0)
                {
                    rb.velocity = Vector2.up * wallSlideSpeedMultiplier;
                }
            }
        }
    }
}
