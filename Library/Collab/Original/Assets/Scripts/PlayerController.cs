﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour {

    #region All variables
    // Basic movement variables
    KeyCode jumpKey = KeyCode.UpArrow;
    public float speed;
    public float maxAirSpeed;
    public float airMovementMultiplier;
    public float jumpForce;
    private float moveInput;
    private float savedAirSpeed;
    private float savedAirSpeedTimer;
    public float maxSavedAirSpeedTime;

    private Rigidbody2D rb;

    private bool facingRight = true;

    // Ground check variables
    private bool isGrounded;
    public Transform groundCheck;
    public float checkRadius;
    public LayerMask whatIsGround;

    //Extra jump variables
    private int extraJumps;
    public int extraJumpsValue;

    // Better jump variables
    public float fallMultiplier;
    public float lowJumpMultiplier;

    private float jumpPressRemember;
    private float jumpPressRememberTime = 0.1f;

    private float groundedRemember;
    private float groundedRememberTime = 0.1f;

    // Wall jumping variables
    private bool isWallSliding;
    private bool allowWallJump = true;
    private GameObject lastWallJumped;
    public Transform leftWallCheck;
    public Transform rightWallCheck;
    public LayerMask whatIsWall;
    public float wallSlideSpeedMultiplier;
    public float wallJumpMultiplier;
    public float wallSlideMinDegrees;
    public float wallSlideMaxDegrees;
    // public float keepVelocityMaxTime;
    // private float keepVelocityTimer;
    private float storedVelocity = 1;
    private int direction;
    private float rotation;
    private float wallJumpPressRemember;
    private float wallJumpPressRememberTime = 0.08f;

    // Moving platform variables
    private Transform currentMovingPlatform;
    private float currentPlatformVelocity;

    // Screens
    public GameObject gameOverScreen;
    public GameObject finishScreen;
    public GameObject HUDScreen;

    // Animation variables 
    private static int AnimatorWalk = Animator.StringToHash("Walk");
    //private static int AnimatorAttack = Animator.StringToHash("Attack");
    private Animator _animator;


    // Death
    private Explodable _explodable;
    public GameObject deathEffect;

    #endregion

    #region Startup and inizialisation

    // Awake() is called after all objects are initialized, before Start()
    // Here you setup the component you are on right now (the "this" object)
    #region Awake
    void Awake()
    {
        _animator = GetComponentInChildren<Animator>();
    }
    #endregion

    // Use this for initialization. Called after Awake()
    // Here you setup things that depend on other components.
    #region Start
    void Start()
    {
        _explodable = GetComponent<Explodable>();
        rb = GetComponent<Rigidbody2D>();

        HUDScreen.SetActive(true);
        gameOverScreen.SetActive(false);

        extraJumps = extraJumpsValue;

        StartCoroutine(Animate());
    }
    #endregion

    #endregion

    #region Physics updates
    // Called once per frame, before Update(). 
    // Do physics calculations here.
    void FixedUpdate()
    {
        moveInput = Input.GetAxis("Horizontal");
        if (isGrounded == false)
        {
            savedAirSpeed = rb.velocity.x;
        }

        #region Get moving platform velocity
        if (currentMovingPlatform != null)
        {
            currentPlatformVelocity = currentMovingPlatform.GetComponent<MovePlayerWithPlatform>().xVelocity;
        }
        else
        {
            currentPlatformVelocity = 0;
        }
        #endregion

        #region Air movement below max speed
        if (isGrounded == false && Mathf.Abs(rb.velocity.x) < maxAirSpeed)
        {
            rb.AddForce(new Vector3(moveInput * speed * airMovementMultiplier, 0, 0), ForceMode2D.Force);
        }
        #endregion

        #region Air movement
        else if (isGrounded == false)
        {
            rb.AddForce(new Vector3(moveInput * speed, 0, 0), ForceMode2D.Force);
        }
        #endregion

        #region Ground movement
        else
        {
            //save air speed velocity for X amount of time when landed and apply it to player
            if (Mathf.Abs(savedAirSpeed) > speed && Mathf.Sign(moveInput) == Mathf.Sign(savedAirSpeed) && moveInput != 0)
            {
                Debug.Log(savedAirSpeed);
                savedAirSpeedTimer += Time.deltaTime;
                if(savedAirSpeedTimer < maxSavedAirSpeedTime)
                {
                    rb.velocity = new Vector2(savedAirSpeed, rb.velocity.y);

                }
                else
                {
                    savedAirSpeed = 0;
                }
            }
            else
            {
                rb.velocity = new Vector2(moveInput * speed + currentPlatformVelocity, rb.velocity.y);
            }
        }
        #endregion

        #region Ground and wall check
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);
        isWallSliding = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsWall) && !isGrounded;
        #endregion

        #region Character direction
        if (facingRight == false && moveInput > 0)
        {
            Flip();
        }
        else if (facingRight == true && moveInput < 0)
        {
            Flip();
        }
        #endregion
    }
    #endregion

    #region Per frame update
    // Update is called once per frame
    void Update () {

        #region Jumping
        #region Extra jump reset
        // Resets extra jumps
        if (isGrounded == true)
        {
            // keepVelocityTimer = 0;
            extraJumps = extraJumpsValue;
            allowWallJump = true;
            lastWallJumped = null;
        }
        if(isWallSliding == true && allowWallJump == true)
        {
            extraJumps = extraJumpsValue;
        }

        if (isWallSliding && rb.transform.rotation.z > 0)
        {
           direction = -1;
        }
        else if (isWallSliding && rb.transform.rotation.z < 0 )
        {
            direction = 1;
        }
        #endregion

        #region Extra jumping
        // Jumping and extra jumping
        /*keepVelocityTimer += Time.deltaTime;
        if (keepVelocityTimer < keepVelocityMaxTime && isWallSliding == false)
        {
            storedVelocity = Mathf.Abs(rb.velocity.x);
        }*/

        //Allow jumping short time after running over a edge
        jumpPressRemember -= Time.deltaTime;
        groundedRemember -= Time.deltaTime;
        wallJumpPressRemember -= Time.deltaTime;

      

        if (Input.GetKeyDown(jumpKey))
        {
            jumpPressRemember = jumpPressRememberTime;
            groundedRemember = groundedRememberTime;
            wallJumpPressRemember = wallJumpPressRememberTime;

            if (isWallSliding == true && allowWallJump == true)
            {
                // keepVelocityTimer += Time.deltaTime;
                rb.AddForce(new Vector3(storedVelocity * wallJumpMultiplier * direction, storedVelocity * wallJumpMultiplier, 0), ForceMode2D.Impulse);
                allowWallJump = false;
                gameObject.transform.rotation = Quaternion.identity; //Resets rotation

                extraJumps--;
                transform.parent = null;
            }


            else if((jumpPressRemember > 0) && isGrounded == false && extraJumps > 0 )
            {

                rb.velocity = new Vector2(rb.velocity.x, 0);
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode2D.Impulse);
                //rb.velocity = Vector2.up * jumpForce;


                gameObject.transform.rotation = Quaternion.identity;  //Resets rotation

                extraJumps--;
                transform.parent = null;
            }

            else if (isGrounded == true)
            {

                rb.velocity = new Vector2(rb.velocity.x, 0);
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode2D.Impulse);
                //rb.velocity = Vector2.up * jumpForce;


                gameObject.transform.rotation = Quaternion.identity;  //Resets rotation
            }

        }

        if ((jumpPressRemember > 0) && (groundedRemember > 0) && isGrounded == true)
        {
            jumpPressRemember = 0;
            groundedRemember = 0;

            rb.velocity = new Vector2(rb.velocity.x, 0);
            rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode2D.Impulse);
            //rb.velocity = Vector2.up * jumpForce;


            gameObject.transform.rotation = Quaternion.identity;  //Resets rotation
        }

        if ((wallJumpPressRemember > 0) && isWallSliding == true && allowWallJump == true)
        {
            wallJumpPressRemember = 0;

            // keepVelocityTimer += Time.deltaTime;
            rb.AddForce(new Vector3(storedVelocity * wallJumpMultiplier * direction, storedVelocity * wallJumpMultiplier, 0), ForceMode2D.Impulse);
            allowWallJump = false;

            gameObject.transform.rotation = Quaternion.identity; //Resets rotation

            extraJumps--;
            transform.parent = null;
        }

        #endregion

        #region Better Jump
        // Better JUMP. Allow smaller jumps as well as faster falling depending on your settings. 
        if (rb.velocity.y < 0) // Faster falling
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
        else if (rb.velocity.y > 0 && !Input.GetKey(jumpKey)) // Smaller jumps
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
        }

        if (isWallSliding == true && isGrounded == false && moveInput != 0)
        {
            // Conversion of values because Unity uses Quaternions for rotation, and us puny humans use real angles...
            rotation = rb.transform.localRotation.eulerAngles.z;
            rotation = rotation > 180 ? rotation - 360 : rotation; 
            if(Mathf.Abs(rotation) <= wallSlideMaxDegrees && Mathf.Abs(rotation) >= wallSlideMinDegrees) // ¯\_(ツ)_/¯
            {
                if(rb.velocity.y < 0)
                {
                    rb.velocity = Vector2.up * wallSlideSpeedMultiplier;
                }
            }
        }
        #endregion
        #endregion
    }
    #endregion

    #region Character Flip
    void Flip()
    {
        facingRight = !facingRight;
        Vector3 Scaler = transform.localScale;
        Scaler.x *= -1;
        transform.localScale = Scaler;
    }
    #endregion

    #region Collisions
    #region Collision Enter
    void OnCollisionEnter2D(Collision2D other)
    {
        #region Hazard
        if (other.gameObject.CompareTag("Hazard"))
        {
            Death();
        }
        #endregion

        #region Platform
        if (other.gameObject.CompareTag("Platform"))
        {
            currentMovingPlatform = other.gameObject.transform;
            // transform.SetParent(currentMovingPlatform);
        }
        #endregion

        #region Ground
        if (LayerMask.LayerToName(other.gameObject.layer) == "Ground")
        {
            isGrounded = true;
        }
        #endregion

        #region Wall
        if (LayerMask.LayerToName(other.gameObject.layer) == "Ground" || (LayerMask.LayerToName(other.gameObject.layer) == "Wall" && isGrounded == false))
        {
            gameObject.transform.rotation = other.transform.rotation;
        }
        

        if (LayerMask.LayerToName(other.gameObject.layer) == "Wall")
        {
            Debug.DrawRay(gameObject.transform.localPosition, other.transform.localPosition, Color.red, 5f);
            if (lastWallJumped != other.gameObject)
            {
                allowWallJump = true;
            }
            /*else
            {
                allowWallJump = true;
            }*/

            lastWallJumped = other.gameObject;
        }
        #endregion
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Hazard"))
        {
            Death();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Hazard"))
        {
            Death();
        }
    }
    #endregion

    #region Collision Stay
    void OnCollisionStay2D(Collision2D other)
    {
        if (LayerMask.LayerToName(other.gameObject.layer) == "Ground")
        {
            isGrounded = true;
        }

        if (LayerMask.LayerToName(other.gameObject.layer) == "Ground" || (LayerMask.LayerToName(other.gameObject.layer) == "Wall" && isGrounded == false))
        {
            if(gameObject.transform.rotation != other.transform.rotation)
            {
                gameObject.transform.rotation = other.transform.rotation;
            }
        }
    }
    #endregion

    #region Collision Exit
    void OnCollisionExit2D(Collision2D other)
    {
        #region Platform
        if (other.gameObject.CompareTag("Platform"))
        {
            currentMovingPlatform = null;
            // transform.parent = null;
        }
        #endregion

        #region Ground
        if (LayerMask.LayerToName(other.gameObject.layer) == "Ground")
        {
            isGrounded = false;
        }
        #endregion  
    }
    #endregion
    #endregion

    #region Death
    public void Death()
    {
        gameOverScreen.GetComponentInParent<screenTimeout>().dead = true;

        _explodable.explode();
        deathEffect.gameObject.transform.parent = GameObject.Find("Player piece").transform;
        deathEffect.SetActive(true);
    }
    #endregion

    #region Finish
    public void Finish()
    {
        gameObject.SetActive(false);
        finishScreen.SetActive(true);
    }
    #endregion

    #region Animate
    IEnumerator Animate()
    {
        //yield return new WaitForSeconds(5f);
        while (true)
        {
            if(rb.velocity.x != 0 && isGrounded)
            {
                _animator.SetBool(AnimatorWalk, true);
                yield return new WaitForSeconds(0.1f);
            }

            if (rb.velocity.x == 0 || !isGrounded)
            {
                _animator.SetBool(AnimatorWalk, false);
                yield return new WaitForSeconds(0.1f);
            }

            /*_animator.transform.localScale = new Vector3(-3, 3, 3);
            yield return new WaitForSeconds(1f);*/

            /*if (Input.GetKey(jumpKey))
            {
                _animator.SetTrigger(AnimatorAttack);
                //yield return new WaitForSeconds(0.1f);
            }*/

            /*_animator.SetTrigger(AnimatorAttack);
            yield return new WaitForSeconds(1f);

            _animator.SetTrigger(AnimatorAttack);
            yield return new WaitForSeconds(5f); */
        }
    }
    #endregion  
}
