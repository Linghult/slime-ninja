﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObject : MonoBehaviour {

    private Vector3 start;
    private Vector3 destination;
    private Vector3 target;

    public float speed;

    public Transform childTransform;
    public Transform destinationObject;

	// Use this for initialization
	void Start () {
        start = childTransform.localPosition;
        destination = destinationObject.localPosition;
        target = destination;
	}
	
	// Update is called once per frame
	void Update () {
        Move();
    }

    private void Move()
    {
        childTransform.localPosition = Vector3.MoveTowards(childTransform.localPosition, target, speed * Time.deltaTime);
        if (Vector3.Distance(childTransform.localPosition, target) <= 0.01)
        {
            ChangeDestination();
        }
            
    }
    
    private void ChangeDestination()
    {
        target = target != start ? start : destination;
    }
}
