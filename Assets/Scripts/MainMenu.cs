﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {
    public GameObject Menu;
    public GameObject OptionsMenu;
    private GameObject Music = null;

	// Use this for initialization
	void Start () {
        if(SceneManager.GetActiveScene().buildIndex == 0)
        {
            Score.TotalScore = 0;
            Score.TotalPossibleScore = 0;
            Score.TotalDeaths = 0;
            Score.CurrentLevelScore = 0;
            Score.CurrentLevelMaxScore = 0;
            Score.CurrentLevelTime = 0;
            Score.TotalTime = 0;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GoBackToMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
        Music = GameObject.Find("Music");
        if(Music != null)
        {
            Music.GetComponent<AudioSource>().Play();
        }
    }

    public void Options()
    {
        Menu.SetActive(false);
        OptionsMenu.SetActive(true);
        GameObject.Find("Improved background graphics").GetComponent<Toggle>().isOn = GameOptions.RickyEnabled;
    }

    public void OptionsBack()
    {
        OptionsMenu.SetActive(false);
        Menu.SetActive(true);
    }

    public void ImprovedBackgroundGraphics()
    {
        if (gameObject.GetComponent<Toggle>().isOn == true)
        {
            GameOptions.RickyEnabled = true;
        }
        else
        {
            GameOptions.RickyEnabled = false;
        }
    }
}
