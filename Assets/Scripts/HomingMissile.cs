﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class HomingMissile : MonoBehaviour
{
    public Transform target;
    private Rigidbody2D rb;
    public float speed;
    public float rotateSpeed;
    public GameObject explosionEffect;
    public AudioSource explosionSound;
    private AudioClip explosionSoundClip;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        explosionSoundClip = explosionSound.clip;
        if(target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player").transform;
        }

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector2 direction = (Vector2)target.position - rb.position;

        direction.Normalize();

        float rotateAmount = Vector3.Cross(direction, transform.up).z;

        rb.angularVelocity = -rotateAmount * rotateSpeed;

        rb.velocity = transform.up * speed;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        AudioSource.PlayClipAtPoint(explosionSoundClip, transform.position);

        if (explosionEffect)
        {
            GameObject newexplosion = (GameObject)Instantiate(explosionEffect, transform.position, transform.rotation);
            Destroy(newexplosion, 3.9f);
        }
        if ( other.gameObject.CompareTag("Player"))
        {
            AudioClip newAudioEffect = gameObject.GetComponent<AudioSource>().clip;
            AudioSource.PlayClipAtPoint(newAudioEffect, transform.position);
        }
        Destroy(gameObject);
    }

}
