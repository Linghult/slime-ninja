﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Score {
    public static int TotalScore { get; set; }
    public static int TotalPossibleScore { get; set; }
    public static int TotalDeaths { get; set; }
    public static int CurrentLevelScore { get; set; }
    public static int CurrentLevelMaxScore { get; set; }
    public static float CurrentLevelTime { get; set; }
    public static float TotalTime { get; set; }
}
