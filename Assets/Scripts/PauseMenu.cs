﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class PauseMenu : MonoBehaviour {

    public GameObject Menu;
    public GameObject OptionsMenu;
    private VideoPlayer Ricky;
    private AudioSource Music;

	// Use this for initialization
	void Start () {
        Ricky = GameObject.Find("Ricky").GetComponentInChildren<VideoPlayer>();
        Ricky.enabled = GameOptions.RickyEnabled;
        if(GameObject.Find("Music") != null)
        {
            Music = GameObject.Find("Music").GetComponent<AudioSource>();
        }
        if(Ricky.enabled == true)
        {
            Music.Pause();
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Pause()
    {
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
            Menu.SetActive(true);
            Music.Pause();
            if(Ricky.enabled == true)
            {
                Ricky.Pause();
            }
        }
        else
        {
            Time.timeScale = 1;
            Menu.SetActive(false);
            OptionsMenu.SetActive(false);
            Ricky.enabled = GameOptions.RickyEnabled;
            if(Ricky.enabled == true)
            {
                Ricky.Play();
            }
            else if(Music != null)
            {
                Music.Play();
            }
        }
    }

    public void Options()
    {
        Menu.SetActive(false);
        OptionsMenu.SetActive(true);
        GameObject.Find("Improved background graphics").GetComponent<Toggle>().isOn = GameOptions.RickyEnabled;
    }

    public void OptionsBack()
    {
        OptionsMenu.SetActive(false);
        Menu.SetActive(true);
    }

    public void ImprovedBackgroundGraphics()
    {
        if(gameObject.GetComponent<Toggle>().isOn == true)
        {
            GameOptions.RickyEnabled = true;
        }
        else
        {
            GameOptions.RickyEnabled = false;
        }
    }
}
