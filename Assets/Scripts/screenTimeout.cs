﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class screenTimeout : MonoBehaviour {

    public bool dead = false;
    private float deathTimer = 0;
    public float gameOverScreenTimeout;

    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update () {

        if ((Input.GetKeyDown(KeyCode.R) || Input.GetKeyDown(KeyCode.Return)) && gameObject.transform.GetChild(0).gameObject.activeSelf == true)
        {
            Score.TotalTime += Score.CurrentLevelTime;
            int scene = SceneManager.GetActiveScene().buildIndex;
            SceneManager.LoadScene(scene);
        }

        if (deathTimer > gameOverScreenTimeout)
        {
            gameObject.transform.GetChild(0).gameObject.SetActive(true);
            dead = false;
        }
        
        if (dead == true)
        {
            deathTimer += Time.deltaTime;
        }
    }
}
