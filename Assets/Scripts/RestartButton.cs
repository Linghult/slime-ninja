﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {

    }

    public void RestartScene()
    {
        Score.TotalTime += Score.CurrentLevelTime;
        int scene = SceneManager.GetActiveScene().buildIndex;
        if(GameOptions.RickyEnabled == false)
        {
            GameObject Music = GameObject.Find("Music");
            if(Music != null)
            {
                Music.GetComponent<AudioSource>().Play();
            }
        }
        SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }
}
