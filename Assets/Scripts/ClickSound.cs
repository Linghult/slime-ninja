﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickSound : MonoBehaviour {

    public AudioClip clickSound;

    private GameObject UISoundObject;
    private AudioSource UIAudioSource;

	// Use this for initialization
	void Start () {
        UISoundObject = GameObject.Find("UI sound");
        if(UISoundObject == null)
        {
            UISoundObject = new GameObject();
            UISoundObject.name = "UI sound";
            UIAudioSource = UISoundObject.AddComponent<AudioSource>();
            UIAudioSource.playOnAwake = false;
            UIAudioSource.SetCustomCurve(AudioSourceCurveType.CustomRolloff, AnimationCurve.Constant(0,1,1));
            UIAudioSource.dopplerLevel = 0;
            UIAudioSource.minDistance = 0;
            UIAudioSource.maxDistance = 5000;

        }
        else
        {
            UIAudioSource = UISoundObject.GetComponent<AudioSource>();
        }
	}
    
    public void PlaySound()
    {
        UIAudioSource.clip = clickSound;
        UIAudioSource.Play();
    }
}
