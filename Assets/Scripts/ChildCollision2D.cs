﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChildCollision2D : MonoBehaviour {
    void OnCollisionEnter2D(Collision2D other)
    {
        gameObject.transform.parent.SendMessage("OnCollisionEnter2D", other);
    }
}
