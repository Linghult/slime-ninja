﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoPointLaser : MonoBehaviour {

    public GameObject emitterA;
    public GameObject emitterB;
    public GameObject emitterADestination;
    public GameObject emitterBDestination;
    public GameObject laser;
    public Light laserLight;
    public AudioSource laserSound;
    public float laserSpeed;
    public float laserInterval;
    public float laserDelayActivation;
    public Color laserActiveColor;
    public Color laserInactiveColor;
 

    private float intervalTimer = 0;
    private bool laserIsOn;
    private LineRenderer line;
    private EdgeCollider2D edgeCollider;
    private List<Vector2> points = new List<Vector2>();
    private Vector3 emitterAStartPos;
    private Vector3 emitterADestinationPos;
    private Vector3 emitterATarget;
    private Vector3 emitterBStartPos;
    private Vector3 emitterBDestinationPos;
    private Vector3 emitterBTarget;
  

    // Use this for initialization
    void Start()
    {
     
        // Make sure the laser is activated
        laser.SetActive(true);
        laserIsOn = true;

        // Add a Line Renderer to the GameObject
        line = laser.GetComponent<LineRenderer>();

        // Set the color of the laser to the defined color
        line.startColor = laserActiveColor;
        line.endColor = laserActiveColor;

        // Set the position of the laser and collider to the emitters' positions
        line.SetPosition(0, emitterA.transform.position);
        line.SetPosition(1, emitterB.transform.position);
        edgeCollider = laser.GetComponent<EdgeCollider2D>();
        points.Add(new Vector2(emitterA.transform.localPosition.x, emitterA.transform.localPosition.y));
        points.Add(new Vector2(emitterB.transform.localPosition.x, emitterB.transform.localPosition.y));
        edgeCollider.points = points.ToArray();

        // Get emitter start positions and destinations
        emitterAStartPos = emitterA.transform.localPosition;
        emitterADestinationPos = emitterATarget = emitterADestination.transform.localPosition;
        emitterBStartPos = emitterB.transform.localPosition;
        emitterBDestinationPos = emitterBTarget = emitterBDestination.transform.localPosition;
    }

    void FixedUpdate()
    {
        if(emitterAStartPos != emitterADestinationPos)
        {
            emitterA.transform.localPosition = Vector3.MoveTowards(emitterA.transform.localPosition, emitterATarget, laserSpeed * Time.deltaTime);
            if (Vector3.Distance(emitterA.transform.localPosition, emitterATarget) <= 0.01)
            {
                emitterATarget = emitterATarget != emitterAStartPos ? emitterAStartPos : emitterADestinationPos;
            }
        }
        if (emitterBStartPos != emitterBDestinationPos)
        {
            emitterB.transform.localPosition = Vector3.MoveTowards(emitterB.transform.localPosition, emitterBTarget, laserSpeed * Time.deltaTime);
            if (Vector3.Distance(emitterB.transform.localPosition, emitterBTarget) <= 0.01)
            {
                emitterBTarget = emitterBTarget != emitterBStartPos ? emitterBStartPos : emitterBDestinationPos;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Check if the GameObjects are not null and laser interval time is larger than 0
        if (emitterA != null && emitterB != null)
        {
            laserDelayActivation -= Time.deltaTime;

            if (laserInterval > 0 && laserDelayActivation <= 0)
            {
                intervalTimer += Time.deltaTime;
                if(intervalTimer > laserInterval)
                {
                    // If laser is on, turn it off
                    if (laserIsOn)
                    {
                        laserIsOn = false;
                        laserSound.enabled = false;
                        intervalTimer = 0;

                        laserLight.enabled = false;
                        line.startColor = laserInactiveColor;
                        line.endColor = laserInactiveColor;

                        edgeCollider.enabled = false;
                    }
                    // If laser is off, turn it on
                    else
                    {
                        laserIsOn = true;
                        laserSound.enabled = true;
                        intervalTimer = 0;

                        laserLight.enabled = true;
                        line.startColor = laserActiveColor;
                        line.endColor = laserActiveColor;

                        edgeCollider.enabled = true;
                    }
                }
            }

            // Checks if laser speed is larger than 0
            if(laserSpeed > 0)
            {
                // Checks if laser has moved
                if (line.GetPosition(0) != emitterA.transform.position || line.GetPosition(1) != emitterB.transform.position)
                {
                    // Update position of the two vertex of the Line Renderer
                    line.SetPosition(0, emitterA.transform.position);
                    line.SetPosition(1, emitterB.transform.position);

                    // Update position of collider
                    points[0] = (new Vector2(emitterA.transform.localPosition.x, emitterA.transform.localPosition.y));
                    points[1] = (new Vector2(emitterB.transform.localPosition.x, emitterB.transform.localPosition.y));
                    edgeCollider.points = points.ToArray();
                    laserSound.transform.localPosition = (points[0] + points[1]) / 2;
                    laserLight.transform.localPosition = (points[0] + points[1]) / 2;

                }
            }
        }
    }


}
