﻿using UnityEngine;
using UnityEngine.UI;

public class Collectibles : MonoBehaviour {

    public int collectibleScore = 0;
    public int totalCollectibles;
    private Text totalScoreText;
    private Text HUDScoreText;

    void Start()
    {
        Score.CurrentLevelScore = 0;
        totalCollectibles = Score.CurrentLevelMaxScore = GameObject.FindGameObjectsWithTag("Collectible").Length;
        totalScoreText = gameObject.GetComponent<PlayerController>().finishScreen.transform.Find("Finish total score").GetComponent<Text>();
        HUDScoreText = GameObject.Find("HUD score text").GetComponent<Text>();
    }

	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Collectible"))
        {
            collectibleScore++;
            Score.CurrentLevelScore++;
            HUDScoreText.text = "Score: " + collectibleScore;
            Destroy(other.gameObject);
		
        }
    }

    public void Finish()
    {
        totalScoreText.text = "Your total score: " + collectibleScore + " / " + totalCollectibles;
    }
}
