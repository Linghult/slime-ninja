﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Credits : MonoBehaviour {
    public Text totalTime;
    public Text totalScore;
    public Text totalDeaths;

	// Use this for initialization
	void Start () {
        totalTime.text += string.Format("{0}:{1:00}", (int)Score.TotalTime / 60, (int)Score.TotalTime % 60);
        totalScore.text += Score.TotalScore + " / " + Score.TotalPossibleScore;
        totalDeaths.text += Score.TotalDeaths;
        GameObject.Find("Music").GetComponent<AudioSource>().Pause();
    }
}
