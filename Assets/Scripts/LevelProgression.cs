﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelProgression : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnNextLevelButtonClick()
    {
        Score.TotalPossibleScore += Score.CurrentLevelMaxScore;
        Score.TotalScore += Score.CurrentLevelScore;
        Score.CurrentLevelMaxScore = Score.CurrentLevelScore = 0;
        int scene = SceneManager.GetActiveScene().buildIndex + 1;
        SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }
}
