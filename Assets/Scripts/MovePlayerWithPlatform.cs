﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayerWithPlatform : MonoBehaviour {

    private GameObject player;
    private Rigidbody2D playerRb;
    private float lastXPosition;
    public float xVelocity;
	
	// Update is called once per frame
	void Update () {
        xVelocity = (transform.position.x - lastXPosition) / Time.deltaTime;
        lastXPosition = transform.position.x;

        if (player != null)
        {
            playerRb.velocity = new Vector2(xVelocity, playerRb.velocity.y);
            //rb.AddForce(new Vector3(platformVelocity, 0, 0), ForceMode2D.Force);
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            player = other.gameObject;
            playerRb = player.GetComponent<Rigidbody2D>();
        }
    }

    void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            player = null;
        }
    }
}
