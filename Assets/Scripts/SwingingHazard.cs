﻿using UnityEngine;

public class SwingingHazard : MonoBehaviour {

    public Rigidbody2D rb;
    public float leftPushLimit;
    public float rightPushLimit;
    public float maxSpeed;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        rb.angularVelocity = maxSpeed;
	}
	
	// Update is called once per frame
	void Update () {
        Push();
	}

    // Pushing pendulum right and left
    public void Push()
    {
        if (transform.rotation.z > 0
            && transform.rotation.z < rightPushLimit
            && rb.angularVelocity > 0
            && rb.angularVelocity < maxSpeed)
        {
            rb.angularVelocity = maxSpeed;
        }
        else if (transform.rotation.z < 0
            && transform.rotation.z > leftPushLimit
            && rb.angularVelocity < 0
            && rb.angularVelocity > -maxSpeed)
        {
            rb.angularVelocity = -maxSpeed;
        }
    }
}
