﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollowScript : MonoBehaviour {
    private GameObject player;
    public float offsetX;
    public float offsetY;

    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
    }
	
	// Update is called once per frame
	void LateUpdate () {
        if (player.activeSelf == true)
        {
            Vector3 setPosition = transform.position;
            setPosition.x = player.transform.position.x + offsetX;
            setPosition.y = player.transform.position.y  + offsetY;
            transform.position = setPosition;
        }
     
        else
        {
            player = GameObject.Find("Player piece");
            if(player == null || player.activeSelf == false)
            {
                player = GameObject.Find("Finish");
            }
            player.AddComponent<AudioListener>();
        }
    }
}
