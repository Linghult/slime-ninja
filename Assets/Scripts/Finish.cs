﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Finish : MonoBehaviour {
    private GameObject finish;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return) && GameObject.Find("Finish screen") != null)
        {
            Score.TotalPossibleScore += Score.CurrentLevelMaxScore;
            Score.TotalScore += Score.CurrentLevelScore;
            Score.CurrentLevelMaxScore = Score.CurrentLevelScore = 0;
            int scene = SceneManager.GetActiveScene().buildIndex + 1;
            SceneManager.LoadScene(scene, LoadSceneMode.Single);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        GameObject.Find("Player").SendMessage("Finish");
    }

}