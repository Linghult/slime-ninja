﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Timer : MonoBehaviour {
    private Text timerText;
    private Text totalTime;
    private float startTime;
    private bool finished = false;
    private string minutes;
    private string seconds;
    public static float time;

    // Use this for initialization
    void Start () {
        startTime = Time.time;
        timerText = GameObject.Find("HUD timer text").GetComponent<Text>();
        totalTime = gameObject.GetComponent<PlayerController>().finishScreen.transform.Find("Finish total time").GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {

        if(finished)
        {
            return;
        }

        time = Score.CurrentLevelTime = Time.time - startTime;
        minutes = ((int)time / 60).ToString();
        seconds = (time % 60).ToString("f2");

        if (time < 60)
        {
            timerText.text = seconds;
        }
        else
        {
            timerText.text = minutes + ":" + seconds;
        }

    }
    public void Finish()
    {
        finished = true;
        Score.TotalTime += Score.CurrentLevelTime;
        Score.CurrentLevelTime = 0;

        if (time < 60)
        {
            totalTime.text = "Your time was: " + seconds+ "s";
        }
        else
        {
            totalTime.text = "Your time was: " + minutes + "m :" + seconds + "s";
        }
    }
}
