﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class missileTrigger : MonoBehaviour {
    public GameObject missilePrefab;
    private Transform spawner;
    public bool fireOnce = true;
    private bool fire = true;

    void Start()
    { 
        spawner = gameObject.transform.GetChild(0).gameObject.transform;
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player") && fire == true)
        {
            Instantiate(missilePrefab, spawner.position, spawner.rotation);
            if (fireOnce == true)
            {
                fire = false;
            }
        }

    }
}
