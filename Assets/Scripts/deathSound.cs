﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (AudioSource))]
public class deathSound : MonoBehaviour {
    private AudioClip deathSoundClip;

    // Use this for initialization
    void Start () {
        deathSoundClip = GetComponent<AudioSource>().clip;
    }


    void OnCollisionEnter2D(Collision2D collision)
    {
		
        if (collision.gameObject.tag == "Player")
        {
            AudioSource.PlayClipAtPoint(deathSoundClip, transform.position);
        }
    }

	void OnTriggerEnter2D(Collider2D collision)
	{

		if (collision.gameObject.tag == "Player")
		{
			AudioSource.PlayClipAtPoint(deathSoundClip, transform.position);
		}
	}
}
