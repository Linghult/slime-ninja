﻿using UnityEngine;

public class SwingingHazard3D : MonoBehaviour
{

    public Rigidbody rb;
    public float leftPushLimit;
    public float rightPushLimit;
    public float maxSpeed;
    private HingeJoint hg;
    private Vector3 maxSpeedVector;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        hg = GetComponent<HingeJoint>();
        maxSpeedVector = hg.axis * maxSpeed;
        rb.angularVelocity = maxSpeedVector;
    }

    // Update is called once per frame
    void Update()
    {
        Push();
    }

    // Pushing pendulum right and left
    public void Push()
    {
        if ((transform.rotation.z > 0 || transform.rotation.z > 0 || transform.rotation.z > 0)
            && (transform.rotation.z < rightPushLimit || transform.rotation.z < rightPushLimit || transform.rotation.z < rightPushLimit)
            && (rb.angularVelocity.z > 0 || rb.angularVelocity.z > 0 || rb.angularVelocity.z > 0)
            && (rb.angularVelocity.z < maxSpeed || rb.angularVelocity.z < maxSpeed || rb.angularVelocity.z < maxSpeed))
        {
            rb.angularVelocity = maxSpeedVector;
        }
        else if ((transform.rotation.z < 0 || transform.rotation.z < 0 || transform.rotation.z < 0)
            && (transform.rotation.z > rightPushLimit || transform.rotation.z > rightPushLimit || transform.rotation.z > rightPushLimit)
            && (rb.angularVelocity.z < 0 || rb.angularVelocity.z < 0 || rb.angularVelocity.z < 0)
            && (rb.angularVelocity.z > maxSpeed || rb.angularVelocity.z > maxSpeed || rb.angularVelocity.z > maxSpeed))
        {
            rb.angularVelocity = -maxSpeedVector;
        }
    }
}
